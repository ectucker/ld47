extends Node


var interval := TAU / 0.2

var time := 0.0

var iterations = 0

var tones_left = 0


onready var win_scene = load("res://src/ui/you_win.tscn")
onready var lose_scene = load("res://src/ui/you_lose.tscn")


var time_to_switch := INF


func _process(delta):
	time += delta
	
	if time > interval:
		tones_left = iterations
		iterations += 1
		time = 0.0
		Soundboard.play("Bell")
	
	if tones_left > 0 and not Soundboard.is_playing("Bell"):
		Soundboard.play("Bell")
		tones_left -= 1
		if iterations == 6 and tones_left == 0:
			if GlobalRefs.player.health > 0.0:
				end_game()
	
	if time_to_switch < 10000:
		time_to_switch -= delta
		if time_to_switch <= 0:
			if GlobalRefs.player.health > 0.0:
				Transition.fade_to(win_scene)
			else:
				Transition.fade_to(lose_scene)
			set_process(false)
	elif GlobalRefs.player.health <= 0.0:
		time_to_switch = 4.0

func end_game():
	GlobalRefs.player.invuln = true
	print("Win!")
	for bullet in get_tree().get_nodes_in_group("EnemyHazards"):
		bullet.queue_free()
	for enemy in get_tree().get_nodes_in_group("Enemies"):
		enemy.health -= 2000.0
		ExplosionManager.add_explosion(128, enemy.global_position)
	GlobalRefs.over = true
	time_to_switch = 4.0
