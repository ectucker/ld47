class_name Enemy
extends KinematicBody2D


const MOVE_ACC := 250.0
const FRICTION_ACC := 600.0

const TURN_ACC := deg2rad(40)
const MAX_TURN := deg2rad(720)

const MAX_SPEED_ATTACK := 200.0
const MAX_SPEED_FLEE := 100.0

const MIN_VELOCITY := 0.3

const RADIUS := 16.0

const CIRCLE_RADIUS := 160.0

const FIRE_RATE := 1.8

var shoot_time = FIRE_RATE * 2

var angular_velocity := 0.0
var velocity := Vector2.ZERO

var health := 100.0 setget set_health

var cleanup_time := 0.0

var target_angle := 0.0

var panic_dir = Vector2.ZERO

onready var anim_tree := find_node("AnimationTree")
onready var anim_state = anim_tree["parameters/state/playback"]

# Holds the linear and angular components calculated by our steering behaviors.
var target_accel := GSAITargetAcceleration.new()

# GSAISteeringAgent holds our agent's position, orientation, maximum speed and acceleration.
onready var agent := GSAISteeringAgent.new()

onready var attack_target := GSAIAgentLocation.new()
onready var flee_target := GSAIAgentLocation.new()

onready var proximity := GSAIRadiusProximity.new(agent, GlobalRefs.all_agents, 100)

onready var attack_blend := GSAIBlend.new(agent)
onready var flee_blend := GSAIBlend.new(agent)

onready var priority := GSAIPriority.new(agent)

var bullet_scene = preload("res://src/enemies/enemy_bullet.tscn")

var dead := false


signal killed(enemy)


func _ready():
	add_to_group("Enemies")
	
	GlobalRefs.register_enemy(self)
	
	target_angle = rand_range(0, TAU)
	
	var player_agent = GlobalRefs.player.agent
	
	# ---------- Configuration for our agent ----------
	agent.linear_speed_max = MAX_SPEED_ATTACK
	agent.linear_acceleration_max = MOVE_ACC
	agent.angular_speed_max = TURN_ACC
	agent.angular_acceleration_max = MAX_TURN
	agent.bounding_radius = RADIUS
	update_agent()
	
	var collisions := GSAIAvoidCollisions.new(agent, proximity)
	
	var pursue := GSAIArrive.new(agent, attack_target)
	
	var face := GSAIFace.new(agent, player_agent)
	face.alignment_tolerance = deg2rad(5)
	face.deceleration_radius = deg2rad(60)
	
	var flee := GSAIFlee.new(agent, flee_target)
	
	var look := GSAILookWhereYouGo.new(agent)
	look.alignment_tolerance = deg2rad(5)
	look.deceleration_radius = deg2rad(60)
	
	flee_blend.add(look, 1)
	flee_blend.add(flee, 1)

	attack_blend.add(face, 1)
	attack_blend.add(pursue, 1)
	
	priority.add(collisions)
	priority.add(flee_blend)
	priority.add(attack_blend)
	
	panic_dir = Vector2(cos(rand_range(0, TAU)), sin(rand_range(0, TAU)))

func _physics_process(delta):	
	if not dead:
		angular_velocity += target_accel.angular * delta
		rotate(angular_velocity)
		
		velocity.x += target_accel.linear.x * delta
		velocity.y += target_accel.linear.y * delta
		if velocity.length_squared() > agent.linear_speed_max * agent.linear_speed_max:
			velocity = velocity.normalized() * agent.linear_speed_max
		elif velocity.length_squared() < MIN_VELOCITY * MIN_VELOCITY:
			velocity = Vector2.ZERO
	else:
		velocity = velocity.normalized() * clamp(velocity.length() - 2000.0 * delta, 0, 500.0)
		pass
	
	velocity = move_and_slide(velocity, Vector2.ZERO, false, 1)
	
	if get_slide_count() > 0:
		panic_dir = Vector2(cos(rand_range(0, TAU)), sin(rand_range(0, TAU)))
	
	if not GlobalRefs.player.attack_mode and not dead:
		shoot_time -= delta
		if shoot_time < 0:
			shoot()
			shoot_time = FIRE_RATE

func _process(delta):
	if not dead:
		update_clock_target(delta)
		update_agent()
		
		priority.calculate_steering(target_accel)

func update_agent() -> void:
	agent.position.x = global_position.x
	agent.position.y = global_position.y
	agent.orientation = rotation
	agent.linear_velocity.x = velocity.x
	agent.linear_velocity.y = velocity.y
	agent.angular_velocity = angular_velocity
	
	if GlobalRefs.player.attack_mode:
		attack_blend.is_enabled = false
		flee_blend.is_enabled = true
		agent.linear_speed_max = MAX_SPEED_FLEE
	else:
		attack_blend.is_enabled = true
		flee_blend.is_enabled = false
		agent.linear_speed_max = MAX_SPEED_ATTACK

func update_clock_target(delta):
	target_angle -= delta
	target_angle = fmod(target_angle, TAU)
	var target = GlobalRefs.player.global_position
	target.x += CIRCLE_RADIUS * cos(target_angle)
	target.y += CIRCLE_RADIUS * sin(target_angle)
	attack_target.position.x = target.x
	attack_target.position.y = target.y
	
	flee_target.position.x = global_position.x + panic_dir.x * 100.0
	flee_target.position.y = global_position.y + panic_dir.y * 100.0

func shoot():
	shoot_time = FIRE_RATE
	anim_tree["parameters/fire/active"] = true

func spawn_projectile():
	if not GlobalRefs.over:
		var bullet = bullet_scene.instance()
		get_parent().add_child(bullet)
		bullet.global_position = global_position
		bullet.direction = Vector2.UP.rotated(rotation)
		Soundboard.play_if_not("CultistShoot")

func set_health(value):
	health = value
	$"Sprite/FlashAnim".play("flash")
	GlobalRefs.player.health += 5.0
	GlobalEffects.hitstop(0.2)
	if health <= 0:
		Soundboard.play("CultistDeath")
		emit_signal("killed", self)
		anim_state.travel("death")
		#visible = false
		collision_layer = 0
		collision_mask = 0
		set_collision_layer_bit(6, true)
		set_collision_mask_bit(6, true)
		dead = true

func knockback(direction):
	velocity = direction * 200.0
