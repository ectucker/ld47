extends Area2D


const MAX_LIFETIME := 20.0

const SPEED := 250.0

const DAMAGE := 10.0

var direction := Vector2.ZERO

var time := 0.0


func _ready():
	connect("body_entered", self, "_on_body_entered")
	add_to_group("EnemyHazards")

func _physics_process(delta):
	global_position += direction * SPEED * delta
	time += delta
	look_at(global_position + direction)
	if time > MAX_LIFETIME:
		queue_free()

func _on_body_entered(body):
	if "health" in body:
		body.health -= DAMAGE
	else:
		Soundboard.play_if_not("MagicalExplosion")
	ExplosionManager.add_evil_explosion(64, global_position)
	queue_free()
