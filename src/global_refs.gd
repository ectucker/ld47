extends Node


var level

var player = null setget set_player

var enemies = []

var all_agents = []

var clock_speed := 0.0

var ai_flee_pos := Vector2.ZERO

var over = false


func set_player(value):
	player = value
	all_agents.append(player.agent)

func register_enemy(enemy):
	enemies.push_back(enemy)
	all_agents.push_back(enemy.agent)
	enemy.connect("killed", self, "remove_enemy")

func remove_enemy(enemy):
	enemies.erase(enemy)
	all_agents.erase(enemy.agent)

func reset():
	level = null
	player = null
	enemies = []
	all_agents = []
	clock_speed = 0.0
	ai_flee_pos = Vector2.ZERO
	over = false
