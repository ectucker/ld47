extends TextureProgress


export(Array, Texture) var textures


const FRAME_TIME = 0.1

var frame = 0

var time = 0.0


func _ready():
	step = max_value / 576

func _process(delta):
	time += delta
	if time > FRAME_TIME:
		frame += 1
		frame = wrapi(frame, 0, textures.size())
		texture_progress = textures[frame]
		time = 0
	
	value = GlobalRefs.player.health
