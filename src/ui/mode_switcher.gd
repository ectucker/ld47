extends ColorRect


func _process(delta):
	if GlobalRefs.player.attack_mode:
		color = Color.red
	else:
		color = Color.blue
