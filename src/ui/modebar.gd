extends TextureProgress


export(Array, Texture) var textures

export(Color) var tint_attack
export(Color) var tint_defense

const FRAME_TIME = 0.1

var frame = 0

var time = 0.0

func _ready():
	max_value = 576
	step = 1

func _process(delta):
	time += delta
	if time > FRAME_TIME:
		frame += 1
		frame = wrapi(frame, 0, textures.size())
		texture_progress = textures[frame]
		time = 0
	
	if GlobalRefs.player.attack_mode:
		value = progress(GlobalRefs.player)
		tint_progress = tint_attack
	else:
		value = progress(GlobalRefs.player)
		tint_progress = tint_defense

func progress(player):
	if player.attack_mode:
		return (player.mode_time / player.ATTACK_LENGTH) * max_value
	else:
		return (1.0 - player.mode_time / player.DEFENSE_LENGTH) * max_value
