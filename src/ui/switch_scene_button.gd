extends Button


export var next: PackedScene


func _ready():
	connect("button_up", self, "pressed")

func pressed():
	GlobalRefs.reset()
	Soundboard.play("MenuClick")
	Transition.fade_to(next)

func play_tick():
	Soundboard.play("ClockTick")
