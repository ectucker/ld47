extends Sprite


var frame_time := 0.048

export var n_frames := 10

var time = 0.0


func _process(delta):
	time += delta
	if time > frame_time:
		frame = wrapi(frame + 1, 0, n_frames)
		time = 0.0
