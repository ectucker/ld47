extends Node2D


var time = 1.0

func _ready():
	GlobalRefs.level = self

func _process(delta):
	time -= delta
	if time <= 0.2:
		var angle = rand_range(0, PI / 2)
		var radius = rand_range(100, 400)
		ExplosionManager.add_explosion(128, global_position + Vector2(radius * cos(angle), -radius * sin(angle)))
		time = rand_range(2.0, 6.0)
