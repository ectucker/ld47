extends TextureRect


export var attack_texture: Texture
export var defense_texture: Texture


func _process(delta):
	if GlobalRefs.player.attack_mode and not texture == attack_texture:
		texture = attack_texture
	
	if not GlobalRefs.player.attack_mode and not texture == defense_texture:
		texture = defense_texture
