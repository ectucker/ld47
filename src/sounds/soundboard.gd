extends Node


func play(name):
	get_node(name).play()

func is_playing(name):
	return get_node(name).is_playing()

func play_if_not(name):
	get_node(name).play_if_not()
