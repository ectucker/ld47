extends Node


func play():
	var sound = get_child(randi() % get_child_count())
	sound.pitch_scale = rand_range(sound.pitch_min, sound.pitch_max)
	sound.play()

func is_playing():
	for child in get_children():
		if child.playing:
			return true
	return false

func play_if_not():
	if not is_playing():
		play()
