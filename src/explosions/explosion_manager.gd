extends Node


var explosion64 = preload("res://src/explosions/explosion64.tscn")
var explosion128 = preload("res://src/explosions/explosion128.tscn")
var explosion256 = preload("res://src/explosions/explosion256.tscn")
var explosion512 = preload("res://src/explosions/explosion512.tscn")

var evil64 = preload("res://src/explosions/evil64.tscn")


func add_explosion(size, position):
	if GlobalRefs.level != null:
		var explosion
		match size:
			64:
				explosion = explosion64.instance()
			128:
				explosion = explosion128.instance()
			256:
				explosion = explosion256.instance()
			512:
				explosion = explosion512.instance()
		GlobalRefs.level.add_child(explosion)
		explosion.global_position = position

func add_evil_explosion(size, position):
	if GlobalRefs.level != null:
		var explosion
		match size:
			64:
				explosion = evil64.instance()
		GlobalRefs.level.add_child(explosion)
		explosion.global_position = position
