extends Sprite


export(Array, Texture) var skins


func _ready():
	texture = skins[randi() % skins.size()]
	$AnimationPlayer.connect("animation_finished", self, "destroy")

func destroy(name):
	queue_free()
