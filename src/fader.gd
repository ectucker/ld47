extends CanvasLayer

var next: PackedScene
var loaded: Node

func fade_to(next_scene: PackedScene):
	next = next_scene
	$AnimationPlayer.play("fade_out")

func transition():
	$Music.stop()
	get_tree().change_scene_to(next)
	$AnimationPlayer.play("fade_in")

func start_music():
	$Music.play()

func _input(event):
	if event.is_action_pressed("dev_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	
	if event.is_action_pressed("dev_exit"):
		get_tree().quit()
