extends StaticBody2D


var time := 0.0

func _physics_process(delta):
	time += delta
	if time > TAU / GlobalRefs.clock_speed:
		queue_free()
