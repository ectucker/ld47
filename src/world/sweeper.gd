extends Area2D


const ROTATION_SPEED := 0.2

const DPS := 10.0

var damagers := []

var cleanup := []

func _ready():
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")
	
	GlobalRefs.clock_speed = ROTATION_SPEED

func _physics_process(delta):
	if not GlobalRefs.over or GlobalRefs.player.health <= 0.0:
		rotate(ROTATION_SPEED * delta)
		
		for entity in damagers:
			entity.health -= DPS * delta
		
		var to_remove = []
		for entity in cleanup:
			if entity.health <= 0.0:
				entity.cleanup_time += delta
				if entity.cleanup_time > PI / 4 / ROTATION_SPEED:
					to_remove.append(entity)
		for entity in to_remove:
			cleanup.erase(entity)
			entity.queue_free()

func _on_body_entered(body):
	if body is Player:
		damagers.push_back(body)
	if body is Enemy:
		cleanup.push_back(body)

func _on_body_exited(body):
	if body is Player:
		damagers.erase(body)
	if body is Enemy:
		cleanup.erase(body)
