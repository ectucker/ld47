extends Node


const ENEMY_INTERVAL := 30 * 4
const OBSTACLE_INTERVAL := 60 * 2

const RADIUS := 900.0
const MIN_DIST := 450.0

export var sweeper_path: NodePath
onready var sweeper = get_node(sweeper_path)

var enemy_scene = preload("res://src/enemies/enemy.tscn")
var obstacle_scene = preload("res://src/world/obstacle.tscn")

var corpses = []

var clicks = 0

func _physics_process(delta):
	if not GlobalRefs.over:
		clicks += 1
		
		if clicks % ENEMY_INTERVAL == 0:
			spawn_enemy()
		
		if clicks % OBSTACLE_INTERVAL == 0:
			spawn_obstacle()

func spawn_enemy():
	var enemy = enemy_scene.instance()
	add_child(enemy)
	enemy.global_position = get_spawn_pos(MIN_DIST, RADIUS)

func spawn_obstacle():
	var obstacle = obstacle_scene.instance()
	add_child(obstacle)
	obstacle.global_position = get_spawn_pos(MIN_DIST, RADIUS)
	obstacle.rotation = rand_range(0, TAU)

func get_spawn_pos(r1, r2):
	var r = rand_range(r1, r2)
	var theta = rand_range(sweeper.rotation - PI / 3, sweeper.rotation + PI / 3)
	var pos = sweeper.global_position
	pos.x += r * cos(theta)
	pos.y += r * sin(theta)
	return pos
