extends Area2D


const FLY_TIME = 0.5
const EXPLODE_TIME := 0.8

const SPEED := 350.0

var direction := Vector2.ZERO

var time := 0.0


func _ready():
	connect("body_entered", self, "_on_body_entered")

func _physics_process(delta):
	time += delta
	if time < FLY_TIME:
		global_position += direction * SPEED * delta
	if time > EXPLODE_TIME:
		explode()

func _on_body_entered(body):
	explode()

func explode():
	ExplosionManager.add_explosion(256, global_position)
	collision_layer = 0
	collision_mask = 0
	$Sprite.visible = false
	$Explosion.direction = direction
	$Explosion.boom()
	Soundboard.play("BombBoom")
	time = -INF
	GlobalEffects.trauma += 0.4
