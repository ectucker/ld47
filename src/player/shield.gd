extends Area2D


var enabled: bool = false setget set_enabled


func _ready():
	connect("area_entered", self, "_area_entered")

func set_enabled(value):
	if value != enabled:
		if value:
			Soundboard.play("ShieldUp")
		else:
			Soundboard.play("ShieldDown")
	enabled = value
	visible = value
	if not value:
		collision_layer = 0
		collision_mask = 0
	else:
		set_collision_layer_bit(2, true)
		set_collision_mask_bit(2, true)

func _area_entered(area):
	if "DAMAGE" in area:
		ExplosionManager.add_evil_explosion(64, area.global_position)
		Soundboard.play_if_not("ShieldBlock")
		area.queue_free()
		var dir = area.global_position - get_parent().global_position
		get_parent().velocity = -dir * 400.0
