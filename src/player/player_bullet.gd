extends Area2D


const MAX_LIFETIME := 20.0

const SPEED := 450.0

const DAMAGE := 100.0

var direction := Vector2.ZERO

var time := 0.0


func _ready():
	connect("body_entered", self, "_on_body_entered")

func _physics_process(delta):
	global_position += direction * SPEED * delta
	time += delta
	if time > MAX_LIFETIME:
		ExplosionManager.add_explosion(64, global_position)
		queue_free()
	look_at(global_position + direction)

func _on_body_entered(body):
	if body is Enemy:
		body.health -= DAMAGE
		body.knockback(direction)
	queue_free()
	ExplosionManager.add_explosion(64, global_position)
