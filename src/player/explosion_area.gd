extends Area2D


const DAMAGE := 100.0

const LIFETIME := 0.1

var started := false
var time := 0.0

var direction := Vector2.ZERO

func _ready():
	connect("body_entered", self, "_on_body_entered")

func _physics_process(delta):
	if started:
		time += delta
	if time > LIFETIME:
		get_parent().queue_free()

func _on_body_entered(body):
	if body is Enemy:
		body.health -= DAMAGE
		body.knockback(direction)

func boom():
	set_collision_layer_bit(1, true)
	set_collision_mask_bit(1, true)
	started = true
