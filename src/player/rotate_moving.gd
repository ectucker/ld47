extends Sprite


var rot_offset


func _ready():
	rot_offset = rotation
	set_material(get_material().duplicate())


func _process(delta):
	global_rotation = get_parent().get_parent().velocity.angle_to(Vector2.RIGHT) + rot_offset
