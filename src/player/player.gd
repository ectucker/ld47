class_name Player
extends KinematicBody2D


const MOVE_ACC := 80000.0
const FRICTION_ACC := 600.0

const MAX_SPEED := 250.0
const MAX_SPEED_SHIELDED := 100.0
const MAX_LOSS := 50.0

const MIN_VELOCITY := 0.3

const ROLL_LENGTH := 0.3
const ROLL_SPEED := 350.0

const FIRE_RATE = 0.2
const BOMB_RATE = 0.8

const DEFENSE_LENGTH := 20.0
const ATTACK_LENGTH := 15.0

const RADIUS := 16

var velocity := Vector2.ZERO
var acceleration := Vector2.ZERO

var input_dir := Vector2.ZERO
var roll_time := 0.0

var shoot_time := 0.0
var bomb_time := 0.0

var attack_mode := false
var mode_time := DEFENSE_LENGTH

var bomb_scene = preload("res://src/player/player_bomb.tscn")
var bullet_scene = preload("res://src/player/player_bullet.tscn")

var health := 100.0 setget set_health

var current_max_speed := MAX_SPEED

var invuln = false

onready var anim_tree := find_node("AnimationTree")
onready var anim_state = anim_tree["parameters/move_state/playback"]

onready var shield = $Shield

onready var agent := GSAISteeringAgent.new()


signal killed()


func _ready():
	GlobalRefs.player = self
	
	# ---------- Configuration for our agent ----------
	agent.linear_speed_max = MAX_SPEED
	agent.linear_acceleration_max = MOVE_ACC
	agent.angular_speed_max = INF
	agent.angular_acceleration_max = INF
	agent.bounding_radius = RADIUS
	
	Transition.start_music()

func _physics_process(delta):
	update_agent()
	
	input_dir.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	input_dir.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	input_dir = input_dir.normalized()
	
	look_at(get_global_mouse_position())
	
	if shield.enabled:
		current_max_speed = MAX_SPEED_SHIELDED
	else:
		current_max_speed = MAX_SPEED
	
	if roll_time < 0:
		if input_dir.length_squared() > 0:
			acceleration = input_dir * MOVE_ACC
		else:
			acceleration = -velocity.normalized() * FRICTION_ACC
	roll_time -= delta
	shoot_time -= delta
	bomb_time -= delta
	mode_time -= delta
	
	if mode_time < 4.5 and mode_time > 2.0:
		Soundboard.play_if_not("Switch")
	if mode_time < 0:
		attack_mode = not attack_mode
		if attack_mode:
			mode_time = ATTACK_LENGTH
		else:
			mode_time = DEFENSE_LENGTH
	
	velocity += acceleration * delta
	if velocity.length_squared() > current_max_speed * current_max_speed:
		if roll_time < 0:
			velocity = velocity.normalized() * current_max_speed
		else:
			velocity = velocity.normalized() * (velocity.length() - MAX_LOSS * delta)
	elif velocity.length_squared() < MIN_VELOCITY * MIN_VELOCITY:
		velocity = Vector2.ZERO
	
	if attack_mode:
		_physics_process_attack(delta)
	else:
		_physics_process_defense(delta)
	
	velocity = move_and_slide(velocity, Vector2.ZERO)

func _physics_process_attack(delta):
	shield.enabled = false
	
	if Input.is_action_pressed("attack_primary") and shoot_time <= 0.0:
		var bullet = bullet_scene.instance()
		get_parent().add_child(bullet)
		bullet.global_position = $BulletSpawn.global_position
		bullet.direction = (get_global_mouse_position() - bullet.global_position).normalized().rotated(rand_range(-PI / 15, PI / 15))
		bullet.look_at(bullet.global_position + bullet.direction)
		velocity = -bullet.direction * 150.0
		shoot_time = FIRE_RATE
		anim_tree["parameters/fire/active"] = true
		Soundboard.play("PlayerShoot")
		if GlobalEffects.trauma < 0.25:
			GlobalEffects.trauma += 0.25
	
	if Input.is_action_pressed("attack_secondary") and bomb_time <= 0.0:
		var bomb = bomb_scene.instance()
		get_parent().add_child(bomb)
		bomb.global_position = $BombSpawn.global_position
		bomb.direction = (get_global_mouse_position() - bomb.global_position).normalized()
		velocity = -bomb.direction * 150.0
		bomb_time = BOMB_RATE
		anim_tree["parameters/bomb/active"] = true
		Soundboard.play("ThrowBomb")
		if GlobalEffects.trauma < 0.2:
			GlobalEffects.trauma += 0.2

func _physics_process_defense(delta):
	shield.enabled = Input.is_action_pressed("attack_primary")
	
	if not shield.enabled:
		if Input.is_action_just_pressed("attack_secondary") and roll_time <= 0.0:
			velocity = ROLL_SPEED * _facing_dir()
			roll_time = ROLL_LENGTH
			acceleration = Vector2.ZERO
			anim_state.travel("roll")
			Soundboard.play("Roll")
			if GlobalEffects.trauma < 0.3:
				GlobalEffects.trauma += 0.3

func update_agent() -> void:
	agent.position.x = global_position.x
	agent.position.y = global_position.y
	agent.orientation = rotation
	agent.linear_velocity.x = velocity.x
	agent.linear_velocity.y = velocity.y
	agent.angular_velocity = 0.0

func _facing_dir():
	return (get_global_mouse_position() - global_position).normalized()

func set_health(value):
	if not invuln and not health <= 0.0:
		var damage = health - value
		
		health = clamp(value, 0.0, 100.0)
		
		if damage > 0:
			GlobalEffects.trauma += damage / 30
			$"Sprite/FlashAnim".play("flash")
			if not Soundboard.is_playing("PlayerDamage"):
				GlobalEffects.hitstop(0.25)
			Soundboard.play_if_not("PlayerDamage")
		
		if health <= 0:
			emit_signal("killed")
			GlobalEffects.hitstop(0.5)
			set_physics_process(false)
			GlobalRefs.over = true
